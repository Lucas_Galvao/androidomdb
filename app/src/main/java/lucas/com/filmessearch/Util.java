package lucas.com.filmessearch;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lucas Galvao Nunes on 20/03/2017.
 */

public class Util {

    static String link = "http://www.omdbapi.com/";
    static  String findName = "?t=";
    static String findYear = "&y=";

    public static String CreateUrlFind(String pNameFilme, String pYear)
    {
        String url = null;
        url = link + findName + pNameFilme + findYear + pYear;
        return url;
    }

    /**
     *Lê um arquivo da web via HTTP e converte o mesmo em String.
     *@param inputStream Stream do arquivo local no aplicativo
     *@return O arquivo convertido em String.
     */
    public static String webToString(InputStream inputStream) {
        InputStream localStream = inputStream;
        String localString = "";
        Writer writer = new StringWriter();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(localStream, "UTF-8"));
            String line = reader.readLine();
            while (line != null) {
                writer.write(line);
                line = reader.readLine();
            }
            localString = writer.toString();
            writer.close();
            reader.close();
            localStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return localString;
    }

    public static FilmeJson convertJSONtoReceitas(String jsonFile){
        FilmeJson filmeJson = null;
        Gson gson = new GsonBuilder().create();
        filmeJson = gson.fromJson(jsonFile, FilmeJson.class);
        return filmeJson;
    }
}
