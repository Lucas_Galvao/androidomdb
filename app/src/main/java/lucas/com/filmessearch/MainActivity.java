package lucas.com.filmessearch;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends Activity {

    public String URL;
    public EditText filmeEdit;
    public EditText anoEdit;

    public TextView tituloText;
    public TextView anoText;
    public TextView avaliacaoText;
    public TextView tempoText;
    public TextView generoText;
    public TextView sobreText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        filmeEdit = (EditText) findViewById(R.id.editFilme);
        anoEdit = (EditText) findViewById(R.id.editAno);

        tituloText = (TextView) findViewById(R.id.textTitulo);
        anoText = (TextView) findViewById(R.id.textAno);
        avaliacaoText = (TextView) findViewById(R.id.textAvaliacao);
        tempoText = (TextView) findViewById(R.id.textTempo);
        generoText = (TextView) findViewById(R.id.textGenero);
        sobreText = (TextView) findViewById(R.id.textSobre);

    }

    public void conectar(View v){

        if(filmeEdit != null && anoEdit != null)
        {
            URL = Util.CreateUrlFind(filmeEdit.getText().toString(),anoEdit.getText().toString());
            new DownloadFromOpenWeather().execute();
        }
        else{
        }
    }

    private class DownloadFromOpenWeather extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(URL);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                String result = Util.webToString(urlConnection.getInputStream());

                return result;
            } catch (Exception e) {
                Log.e("Error", "Error ", e);
                return null;
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FilmeJson filme = Util.convertJSONtoReceitas(s);
            tituloText.setText(filme.getTitle());
            anoText.setText(filme.getYear());
            avaliacaoText.setText(filme.getRated());
            tempoText.setText(filme.getRuntime());
            generoText.setText(filme.getGenre());
            sobreText.setText(filme.getPlot());



        }
    }
}
